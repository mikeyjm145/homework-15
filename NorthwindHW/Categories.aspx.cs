﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for the Categories page
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public partial class Categories : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the RowUpdated event of the gvCategory control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvCategory_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblError.Text = "";
            return;
        }

        this.lblError.Text = "A database method has occurred.<br/><br/>" +
                             "Message: " + e.Exception.Message;
        e.ExceptionHandled = true;
        e.KeepInEditMode = true;
    }
}