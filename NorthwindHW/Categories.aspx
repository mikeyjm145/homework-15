﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Categories.aspx.cs" Inherits="Categories" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 15: Northwind</title>
    <link href="Main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        &nbsp;<img alt="Northwind Solutions" src="Images/Northwind.jpg" /><br />
    </header>
    <section>
    <form id="form1" runat="server">
        &nbsp;<br />
        
        
        <asp:Label ID="lblCategoyMaintenance" runat="server" Font-Bold="True" Font-Size="Larger" Text="Categories Maintenance"></asp:Label>
        <br />
        <asp:GridView ID="gvCategory" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CategoryID" DataSourceID="sdsNorthwind" ForeColor="#333333" GridLines="None" OnRowUpdated="gvCategory_RowUpdated" AllowSorting="True">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="CategoryID">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("CategoryID") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("CategoryID") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name" SortExpression="CategoryName">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtCategory" runat="server" Text='<%# Bind("CategoryName") %>' Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ControlToValidate="txtCategory" Display="Dynamic" ErrorMessage="Please enter value into field." ForeColor="Red" ValidationGroup="GV">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("CategoryName") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description" SortExpression="Description">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("Description") %>' Width="300px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" Display="Dynamic" ErrorMessage="Please enter value into field." ForeColor="Red" ValidationGroup="GV">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:CommandField ShowEditButton="True" ValidationGroup="GV" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <asp:ValidationSummary ID="vsGVCategory" runat="server" ForeColor="Red" HeaderText="Please correct the following errors." ValidationGroup="GV" />
        <br />
        <asp:Label ID="lblInstructions" runat="server" Text="To create a new category, enter the category information and click Add New Category"></asp:Label>
        <br />
        <asp:SqlDataSource ID="sdsNorthwind" runat="server" ConnectionString="<%$ ConnectionStrings:CategoryMaintenanceConnectionString %>" DeleteCommand="DELETE FROM [tblCategories] WHERE [CategoryID] = ?" InsertCommand="INSERT INTO tblCategories(CategoryID, CategoryName, Description) VALUES (?, ?, ?)" ProviderName="<%$ ConnectionStrings:CategoryMaintenanceConnectionString.ProviderName %>" SelectCommand="SELECT [CategoryID], [CategoryName], [Description] FROM [tblCategories]" UpdateCommand="UPDATE [tblCategories] SET [CategoryName] = ?, [Description] = ? WHERE [CategoryID] = ?">
            <DeleteParameters>
                <asp:Parameter Name="CategoryID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="CategoryID" Type="Int32" />
                <asp:Parameter Name="CategoryName" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="CategoryName" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="CategoryID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
        <asp:DetailsView ID="dvCategory" runat="server" AutoGenerateRows="False" CellPadding="4" DataKeyNames="CategoryID" DataSourceID="sdsNorthwind" DefaultMode="Insert" ForeColor="#333333" GridLines="None" Height="50px" Width="244px">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
            <EditRowStyle BackColor="#2461BF" />
            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
            <Fields>
                <asp:TemplateField HeaderText="ID" SortExpression="CategoryID">
                    <InsertItemTemplate>
                        <asp:TextBox ID="txtCategoryID" runat="server" Text='<%# Bind("CategoryID", "{0:N}") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCategoryID" runat="server" ControlToValidate="txtCategoryID" Display="Dynamic" ErrorMessage="Please enter value into field." ForeColor="Red" ValidationGroup="DV">*</asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("CategoryID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name" SortExpression="CategoryName">
                    <InsertItemTemplate>
                        <asp:TextBox ID="txtCategoryName" runat="server" Text='<%# Bind("CategoryName", "{0}") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ControlToValidate="txtCategoryName" Display="Dynamic" ErrorMessage="Please enter value into field." ForeColor="Red" ValidationGroup="DV">*</asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("CategoryName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description" SortExpression="Description">
                    <InsertItemTemplate>
                        <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" Display="Dynamic" ErrorMessage="Please enter value into field." ForeColor="Red" ValidationGroup="DV">*</asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField NewText="Add New Category" ShowInsertButton="True" ValidationGroup="DV" />
            </Fields>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
        </asp:DetailsView>
        <asp:ValidationSummary ID="vsDVCategory" runat="server" ForeColor="Red" HeaderText="Please correct the following errors:" ValidationGroup="DV" />
        
        
    </form>
    </section>
</body>
</html>
